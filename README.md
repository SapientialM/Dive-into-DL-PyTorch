本项目 copy from https://github.com/ShusenTang/Dive-into-DL-PyTorch

本项目将《动手学深度学习》 原书中MXNet代码实现改为PyTorch实现。原书作者：阿斯顿·张、李沐、扎卡里 C. 立顿、亚历山大 J. 斯莫拉以及其他社区贡献者，GitHub地址：https://github.com/d2l-ai/d2l-zh

#### [跳转到介绍](./docs/README.md)